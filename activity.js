db.course_bookings.inserMany([
	{ "courseId": "C001", "studentId": "S004", "isCompleted": true},	
	{ "courseId": "C002", "studentId": "S001", "isCompleted": false},	
	{ "courseId": "C001", "studentId": "S003", "isCompleted": true},	
	{ "courseId": "C003", "studentId": "S002", "isCompleted": false},	
	{ "courseId": "C001", "studentId": "S002", "isCompleted": true},	
	{ "courseId": "C004", "studentId": "S003", "isCompleted": false},	
	{ "courseId": "C002", "studentId": "S004", "isCompleted": true},	
	{ "courseId": "C003", "studentId": "S007", "isCompleted": false},	
	{ "courseId": "C001", "studentId": "S005", "isCompleted": true},	
	{ "courseId": "C004", "studentId": "S008", "isCompleted": false},	
	{ "courseId": "C001", "studentId": "S013", "isCompleted": true},	
]);

// Count the completed courses of student S013
db.course_bookings.aggregate(
	[ 
		{$match:  {"isCompleted": true, "studentId": "S013"}},
		{$group: {_id: null, count: {$sum: 1}}}
]);


// Students not yet completed and Course ID is hidden
db.course_bookings.aggregate(
	[ 
		{$match:  {"isCompleted": false}},
		{$project: { "courseId": 0}}
]);

// CourseId in descending order while studentId in ascending order

db.course_bookings.aggregate(
	[ 
		{ $sort: { "courseId": -1, "studentId": 1}}
]);